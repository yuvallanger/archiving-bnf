import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

class ArchiveItem(scrapy.Item):
    url = scrapy.Field()
    destination_urls = scrapy.Field()


class DeepFreezeItSpider(CrawlSpider):

    name = "deepfreezeit"
    allowed_domains = ["deepfreeze.it"]
    start_urls = ["http://deepfreeze.it"]
    rules = [Rule(LinkExtractor(), "parse_archive")]

    def parse_archive(self, response):
        archive = ArchiveItem()
        archive["url"] = response.url
        archive["destination_urls"] = response.xpath("//@href").re(r"https?://archive.is/([a-zA-Z0-9]+)")
        return archive
